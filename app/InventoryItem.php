<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    protected $table ='inventoryitem';
    protected $primaryKey = 'itemid';

    // timestamps
    public $timestamps =false;
}
    

