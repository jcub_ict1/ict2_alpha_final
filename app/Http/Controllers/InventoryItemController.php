<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\InventoryItem;
use Excel;
use Validator;
use Importer;


class InventoryItemController extends Controller
{
    function index()
    {
     $data = DB::table('inventoryitem')->orderBy('itemid', 'DESC')->get();
     return view('import_excel', compact('data'));
    }

    public function downloadData($type)
    {
        $data = InventoryItem::get()->toArray();

        return Excel::create('excel_data', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importData(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);

        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();

        if($data->count()){
            foreach ($data as $key => $value) {
                $arr[] = [
                'itemid'  => $value->itemid,
                'amountexisting'   => $value->amountexisting,
                'amountshown'   => $value->amountshown,
                'pricebought'    => $value->pricebought,
                'priceselling'  => $value->priceselling,
                'name'   => $value->name,
                'description'  => $value->description,
                'gallery'   => $value->gallery,
                'expirydate'   => $value->expirydate,
                'datebought'    => $value->datebought,
                'catid'  => $value->catid,
                'iid'   => $value->iid                 
                ];
            }

            if(!empty($arr)){
                InventoryItem::insert($arr);
            }
        }

        return back()->with('success', 'Insert Record successfully.');
    }
}



