<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Stocks;
use Validator;
use Auth;
use App\Organizations;
use App\Inventory;
use App\Category;
use App\Inventoryitem;




class StocksController extends Controller
{
    function index()
    {


        $orgId = Auth::user()->organization_id;
        $org = Organizations::where('oid', $orgId)->first();


        // dd($org);
        //$data = DB::table('inventoryitem')->orderBy('itemid', 'DESC')->get();

        $data =DB::table('inventoryitem')
        ->join('category', 'inventoryitem.catid', '=', 'category.catid')
        ->join('inventory', 'category.iid', '=', 'inventory.iid')
        ->join('branches', 'inventory.bid', '=', 'branches.bid')
        ->join('organizations', 'branches.oid', '=', 'organizations.oid')
        ->select('inventoryitem.itemid','inventoryitem.name as itemname','inventoryitem.description'
           ,'inventoryitem.gallery','inventoryitem.expirydate','inventoryitem.datebought','inventoryitem.minimumstocklevel'
           ,'category.name as categoryname')
        ->where('organizations.oid', $orgId)
        ->get();
        //return view('stocks', compact('data'));   

 
        return view('stocks', compact('data'), array(
            'org' => $org
        ));  
       

    }





 }   