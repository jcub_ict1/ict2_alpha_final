@extends('layouts.app')
@section('title','List of ')

@section('content')
<h1> Organization Lists</h1>
<table class="table table-light">
    <tbody>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Contact no</td>
            <td>headquarter</td>  
            <td>theme color</td>  
        </tr>
        @foreach ($organizations as $orgList)
        <tr>
        
            <td>{{$orgList->oid}}</td>
            <td><a href = "/organizations/{{$orgList->oid}}">{{$orgList->name}}</td></a>
            <td>{{$orgList->contactno1}}</td>
            <td>{{$orgList->headquarter}}</td>
            <td>{{$orgList->themecolor}}</td>
        </tr>
        @endforeach
    </tbody>
@endsection
