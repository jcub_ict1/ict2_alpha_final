@extends('layouts.app')

@section('content')
  
  <div class="container">
   <h3 align="center">Stocks Available for</h3>
    <a href="../organizations/{{$org->oid}}"> <u><h3 align="center"> {{ $org->name }}</h3></u> </a>

    <br />
    </div>
    
   <br />
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title">Stock Details</h3>
    </div>
    <div class="panel-body">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
      <a href="../organizations/{{$org->oid}}"><u><h2 class="panel-title">Organization ID :: {{ $org -> oid }}</h3></u></a>
      <u><h2 class="panel-title">Admin :: {{ Auth::user()->name }}</h3></u>

       <tr>
        <th>Item ID</th>
        <th>Item Name</th> 
        <th>Description</th>
        <th>Gallery</th>
        <th>Expiry Date</th>
        <th>Date Bought</th>
        <th>Minimum Stock Level</th>  
        <th>Category Name</th>    
       </tr>
       @foreach($data as $row)
       <tr>
        <td>{{ $row->itemid }}</td>
        <td>{{ $row->itemname }}</td>  
        <td>{{ $row->description }}</td>
        <td>{{ $row->gallery }}</td>
        <td>{{ $row->expirydate }}</td>
        <td>{{ $row->datebought }}</td> 
        <td>{{ $row->minimumstocklevel }}</td>  
        <td>{{ $row->categoryname }}</td>     
       </tr>
       @endforeach
      <br/>
      <br/>
      </table>

     
     </div>
    </div>
   </div>
  </div>


@endsection

