<?php $__env->startSection('content'); ?>
<h1>Branches</h1>
<br>
<h3> Description about Branches</h3>
<?php if(count($branch)>0): ?>
<table class = "table">
        <thead>
                <tr>
                <th>S.N.</th>
                <th>Organization Id</th>
                <th>Address Id</th>
                <th>Post Office Box</th>
                <th>Contact Number</th>
                
                </tr>
            </thead>
    <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tbody>
            <tr>
            <td> <h3><?php echo e($branch->bid); ?></h3></td>
            <td> <h3 ><a href ="/branch/<?php echo e($branch->bid); ?>" ><?php echo e($branch->oid); ?></a></h3></td>
                        <td> <h3 ><?php echo e($branch->addressid); ?></h3></td>
            <td> <h3><?php echo e($branch->pobox); ?></h3></td>
			<td> <h3><?php echo e($branch->contactno); ?></h3></td>
                        </tr>
                       
        </tbody>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
           
    
    <?php else: ?>
    No branch found 
<?php endif; ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final1\ICT_Alpha_Final_Praj\resources\views/branch_pages/branch_index.blade.php ENDPATH**/ ?>