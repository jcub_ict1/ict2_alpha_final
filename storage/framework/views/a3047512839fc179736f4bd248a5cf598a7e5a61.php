<?php echo strip_tags($header); ?>


<?php echo strip_tags($slot); ?>

<?php if(isset($subcopy)): ?>

<?php echo strip_tags($subcopy); ?>

<?php endif; ?>

<?php echo strip_tags($footer); ?>

<?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\vendor\laravel\framework\src\Illuminate\Mail/resources/views/text/layout.blade.php ENDPATH**/ ?>