<?php $__env->startSection('content'); ?>

<hr>

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">
 <div class="panel-heading">
 <b><h1>Customers Data</h1></b>
 </div>
 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> First Name </th>
 <th> Middle Name</th>
 <th> Last Name </th>
 <th> Date of Birth </th>
 <th> Gender</th>
 <th> Email </th>
 

 </thead>
<tbody>
<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <td><?php echo e($customer->fname); ?></td>            
    <td><?php echo e($customer->mnames); ?></td>
    <td><?php echo e($customer->lname); ?></td>            
    <td><?php echo e($customer->dob); ?></td>
    <td><?php echo e($customer->gender); ?></td>            
    <td><?php echo e($customer->email); ?></td>
          

    <td> <p><a href="<?php echo e(route('customers.show', $customers->cid)); ?>" class="btn btn-info">View customer</a>
        <a href="<?php echo e(route('customers.edit', $customers->cid)); ?>" class="btn btn-primary">Edit </a>
    </p></td> 
    </tr> 
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</tbody>
</table>
</div>
</section>
</div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final1\ICT_Alpha_Final_Praj\resources\views/customers/index.blade.php ENDPATH**/ ?>