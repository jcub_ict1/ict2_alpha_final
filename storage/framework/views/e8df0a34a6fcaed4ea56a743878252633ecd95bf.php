<?php $__env->startSection('title'); ?>
    Edit Organizations
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class = "col-12">
        <h1><?php echo e($organizations->name); ?></h1>
    </div> 
    <div class="row">
        <div class="col-12">
            <form action = "/organizations/<?php echo e($organizations->oid); ?>" method="POST" enctype="multipart/form-data">
                <?php echo method_field('PATCH'); ?>
                <?php echo $__env->make('organizations.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <button class = "btn btn-primary">Save organization</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final1\ICT_Alpha_Final_Praj\resources\views/organizations/edit.blade.php ENDPATH**/ ?>