<?php $__env->startSection('content'); ?>
    <h1>Create Admin</h1>
    
    <?php echo Form::open(['action'=> 'adminController@store','method'=>'POST']); ?>



	<div class = "form-group">
        <?php echo e(Form::label('fname','First name')); ?>

        <?php echo e(Form::text('fname','',['class'=>'form-control','placeholder'=>'First name', 'required'])); ?>

    </div>

<div class = "form-group">
                <?php echo e(Form::label('mname','Middle name')); ?>

                <?php echo e(Form::text('mname','',['class'=>'form-control','placeholder'=>'Middle name'])); ?>

            </div>
    
        
        <div class = "form-group">
            <?php echo e(Form::label('lname','Last name')); ?>

            <?php echo e(Form::text('lname','',['class'=>'form-control','placeholder'=>'Last name', 'required'])); ?>

        </div>
    
        
        
                
        
    
            
        
        <div class = "form-group">
                        <?php echo e(Form::label('gender','Gender')); ?>

                        <?php echo e(Form::text('gender','',['class'=>'form-control','placeholder'=>'gender', 'required'])); ?>

                </div>
        
        
        
        <div class = "form-group">
                <?php echo e(Form::label('nationality','Nationality')); ?>

                <?php echo e(Form::text('nationality','',['class'=>'form-control','placeholder'=>'nationality', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                <?php echo e(Form::label('proflepic','Profile pic')); ?>

                <?php echo e(Form::text('profilepic','',['class'=>'form-control','placeholder'=>'Picture', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                    <?php echo e(Form::label('jobposition','Job Position')); ?>

                    <?php echo e(Form::text('jobposition','',['class'=>'form-control','placeholder'=>'job position', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                    <?php echo e(Form::label('email','Email')); ?>

                    <?php echo e(Form::text('email','',['class'=>'form-control','placeholder'=>'Email', 'required'])); ?>

        </div>
        
        <div class = "form-group">
                <?php echo e(Form::label('password','Password')); ?>

                <?php echo e(Form::text('password','',['class'=>'form-control','placeholder'=>'Password', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                <?php echo e(Form::label('postaladdress','Postal Address')); ?>

                <?php echo e(Form::text('postaladdress','',['class'=>'form-control','placeholder'=>'Postal Address', 'required'])); ?>

        </div>  
    
        
        <div class = "form-group">
                <?php echo e(Form::label('address','Address')); ?>

                <?php echo e(Form::text('address','',['class'=>'form-control','placeholder'=>'Address', 'required'])); ?>

        </div> 
    
        <?php echo e(Form::submit('Submit',['class'=>'btn btn-primary'])); ?>

        <?php echo Form::close(); ?>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/admin_pages/admin_create.blade.php ENDPATH**/ ?>