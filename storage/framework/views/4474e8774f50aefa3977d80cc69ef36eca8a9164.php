<?php $__env->startSection('title'); ?>
    Show Organizations
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<h3> Information about Organizations</h3>
    <form action = "/organizations/<?php echo e($organizations->oid); ?>/edit">
        <button class = "btn btn-primary">Edit</button>
    </form>
    <form action = "/organizations/<?php echo e($organizations->oid); ?>" method = "POST">
        <?php echo method_field('delete'); ?>
        <?php echo csrf_field(); ?>
        <button value="delete" class="btn btn-danger">Delete</button>
    </form>
    <br/>
<p><strong>ID:</strong><?php echo e($organizations->oid); ?></p>
<p><strong>Name:</strong><?php echo e($organizations->name); ?></p>
<p><strong>Mobile No:</strong><?php echo e($organizations->contactno1); ?></p>
<p><strong>Phone No:</strong><?php echo e($organizations->contactno2); ?></p>
<p><strong>Headquarter:</strong><?php echo e($organizations->headquarter); ?></p>
<p><strong>About:</strong><?php echo e($organizations->about); ?></p>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/organizations/show.blade.php ENDPATH**/ ?>