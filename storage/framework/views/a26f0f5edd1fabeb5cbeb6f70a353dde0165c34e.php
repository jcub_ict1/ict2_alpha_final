<?php $__env->startSection('content'); ?>
  
  <div class="container">
   <h3 align="center">Stocks Available for</h3>
    <a href="../organizations/<?php echo e($org->oid); ?>"> <u><h3 align="center"> <?php echo e($org->name); ?></h3></u> </a>

    <br />
    </div>
    
   <br />
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title">Stock Details</h3>
    </div>
    <div class="panel-body">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
      <a href="../organizations/<?php echo e($org->oid); ?>"><u><h2 class="panel-title">Organization ID :: <?php echo e($org -> oid); ?></h3></u></a>
      <u><h2 class="panel-title">Admin :: <?php echo e(Auth::user()->name); ?></h3></u>

       <tr>
        <th>Item ID</th>
        <th>Item Name</th> 
        <th>Description</th>
        <th>Gallery</th>
        <th>Expiry Date</th>
        <th>Date Bought</th>
        <th>Minimum Stock Level</th>  
        <th>Category Name</th>    
       </tr>
       <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <tr>
        <td><?php echo e($row->itemid); ?></td>
        <td><?php echo e($row->itemname); ?></td>  
        <td><?php echo e($row->description); ?></td>
        <td><?php echo e($row->gallery); ?></td>
        <td><?php echo e($row->expirydate); ?></td>
        <td><?php echo e($row->datebought); ?></td> 
        <td><?php echo e($row->minimumstocklevel); ?></td>  
        <td><?php echo e($row->categoryname); ?></td>     
       </tr>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <br/>
      <br/>
      </table>

     
     </div>
    </div>
   </div>
  </div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/stocks.blade.php ENDPATH**/ ?>